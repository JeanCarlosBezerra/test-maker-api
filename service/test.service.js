const repository = require('../repository/test.repository')
const ValidationException = require('../exception/validation.exception')

exports.findAll = async () => {
    return await repository.findAll()
}

exports.save = async (test) => {

    if (test == null) {
        throw new ValidationException("Teste é obrigatório")
    }

    if (test.name == null || test.description === "") {
        throw new ValidationException("Descrição é obrigatório")
    }

    if (test.creationDate = null || test.creationDate === "" ) {
        throw new ValidationException("Teste é obrigatório")
    }

    await repository.save(test)
}