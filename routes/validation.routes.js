const service = require('../service/validation.service')

module.exports = (app) => {

    app.post('/test/grade', async (req, res) => {
        await service.save(req.body)
        res.end()
    })

}