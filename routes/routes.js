const testRoutes = require("./test.routes")
const questionRoutes = require("./question.routes")
const validationRoutes = require("./validation.routes.js")

module.exports = (app) => {

    testRoutes(app)
    questionRoutes(app)
    validationRoutes(app)
    
}